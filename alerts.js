$(document).ready(function(){


    window.Alert = Backbone.Model.extend({
	     defaults: {
         id: 0,
	       message: null,
        messageType: null
	    },
      url : function() {
        return 'http://private-d7f0fa-sheldonbarnes.apiary-mock.com/alerts';
      }
    });

    window.UserInfo = Backbone.Model.extend({
      url : function() {
        return 'http://private-d7f0fa-sheldonbarnes.apiary-mock.com/user1';
      }
    });

    window.Alerts = Backbone.Collection.extend({
	     model: Alert,
       url: function() {
         return 'http://private-d7f0fa-sheldonbarnes.apiary-mock.com/alerts1';
       },
	     initialize: function(alerts) {
	     }
    });

    window.UserInfoView = Backbone.View.extend({

      el: $("div#userinfo"),

      events: {
        "click button[id='user-info-save']": "saveUserInfo"
      },
      saveUserInfo: function(ev) {
        console.log('This is clicked');
        this.model.set('firstName', $('input[id=firstName]').val() );
        this.model.set('lastName', $('input[id=lastName]').val() );
        this.model.set('email', $('input[id=emailAddress]').val() );

        this.model.save ( this.model.toJSON(), {
            success        : this.onSaveSuccess
        });

        $("div#success").show();

        console.log('This is the model for the userModel' + JSON.stringify(this.model));
        //this.render();
      },
      initialize: function() {

        $("div#success").hide();


      },
      render: function() {
        console.log('I am in the render for the UserInfoView' + JSON.stringify(this.model));
        console.log('I am in the render')

        $(this.el).html(_.template($('#userinfo-template').html(),
        {
  		      id          : this.model.id,
  		      firstName     : this.model.get('firstName'),
  		      lastName : this.model.get('lastName'),
            email : this.model.get('email_address')
        }));
        return this;
      }
    });

    // The View for an Individual Alert
    window.AlertView = Backbone.View.extend({

  	   initialize: function() {
         this.model.on('change', this.render, this);
  	   },
  	   render: function() {
        $(this.el).html(_.template($('#alerts-template').html(),
        {
  		      id          : this.model.id,
  		      message     : this.model.get('message'),
  		      messageType : this.model.get('messageType')
        }));
        return this;
  	   }

     });


window.AlertsView = Backbone.View.extend({
	el: $("div#contents"),
  events : {
      "click div[id='deleteButton']": "deleteAlert",
      "click button" : "addAlert"
  },
  deleteAlert : function(ev) {
    var alertId = $(ev.currentTarget).data('id');

    var thisAlert = this.collection.get(alertId);
    thisAlert.destroy();

    this.collection.remove(alertId);
    this.render();
  },
  addAlert : function(event) {
    var newAlert = new window.Alert({
              id: Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000,
  	          message: $('input[id=newAlertText]').val(),
              messageType: "fa-bell-o"
  	});
    newAlert.save();  //saves the alert
    this.collection.add(newAlert); // add it to the collection
    this.render(); //render the view
  },


	render: function() {

    $("div#alerts").empty();
	  this.collection.each(function(alert) {

		    var alertView = new AlertView({model: alert});
		      $("div#alerts").append(alertView.render().el);
    });


	}

    });


});
